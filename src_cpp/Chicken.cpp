//
// Created by aniko on 2020. 02. 19..
//

#include "Chicken.h"

void Chicken::Spawn(mutex* m, bool tester){
    *ispoop=true;
    while((*ispoop)==true)
    {
        int timeinterval = rand() % 8 + 3;
//        cout<<"Pooptime "<<name<<": "<<timeinterval<<endl;
        if(!tester) {
            this_thread::sleep_for(chrono::seconds(timeinterval));
        }
        else{
            this_thread::sleep_for(chrono::milliseconds (10));
        }
        m->lock();
        (*eggs)++;
        m->unlock();
//        cout<<"Poop by "+name<<": "<<*eggs<<endl;
    }
}

Chicken::Chicken(string cname): name(cname){
}

string Chicken::GetName() {
    return name;
}

int Chicken::GetEgg() {
    return *eggs;
}

bool Chicken::IsPoop() {
    return *ispoop;
}

void Chicken::SetPoop(bool poop) {
    *ispoop=poop;
}

void Chicken::Clean() {
    delete ispoop;
    delete eggs;
}

