//
// Created by aniko on 2020. 02. 19..
//

#ifndef CHICKEN_QCHICKEN_H
#define CHICKEN_CHICKEN_H

#include <iostream>
#include <chrono>
#include <thread>
#include <mutex>

using namespace std;

class Chicken {
public:
    Chicken(string cname);
    void Spawn(mutex* m, bool tester=false);
    string GetName();
    int GetEgg();
    bool IsPoop();
    void SetPoop(bool poop);
    void Clean();
private:
    string name;
    int *eggs = new int(0);
    bool *ispoop=new bool(false);
};


#endif //CHICKEN_QCHICKEN_H
