#include <iostream>
#include "Chicken.h"
#include <thread>
#include <mutex>
#include <vector>
#include <cstdlib>
#include <ctime>

using namespace std;

void printHello(bool init){
    cout<<"     __//"<<endl;
    cout<<"    /.__.\\ "<<endl;
    if(init){
        cout<<"    \\ \\/ /   Hello Chicken!"<<endl;
        cout<<" ,__/    \\ "<<endl;
        cout<<"  \\-      )"<<endl;
    }
    else{
        cout<<"    \\ \\/ /   Bye Chicken!"<<endl;
        cout<<" ,__/    \\/\\ "<<endl;
        cout<<"  \\-      _/"<<endl;
    }
    cout<<"   \\_____/ "<<endl;
    cout<<"_____|_|______"<<endl;
    cout<<"     \" \" "<<endl;
}

int main(int argc, char *argv[])
{
    srand( (unsigned)time(NULL) );
    mutex mut;
    string menu="";
    vector<Chicken> pips;
    std::vector<std::thread> threads;
    int i=1;

    printHello(true);
    do {
        cout << endl<<"Please select one of: " << endl;
        cout << "1 : Plant chicken" << endl;
        cout << "2 : Status" << endl;
        cout << "3 : Stop the hens" << endl;
        cout << "4 : Shoo!" << endl;
        cin >> menu;

        if(menu=="1"){ // Start a pooping chick
            string name="Chicken_"+std::to_string(i);
            Chicken c(name);
            threads.emplace_back(std::thread(&Chicken::Spawn, c, &mut, false));
            pips.push_back(c);
            i++;
        }
        if(menu=="2"){ // Get chikens status
            cout<<"Chicken    Status   Eggs"<<endl;
            if(pips.size()==0){cout<<"There are no chickens!"<<endl;}
            else {
                for (Chicken& c: pips) {
                    cout << c.GetName()<<" "<<(c.IsPoop()?"(pooping)":"(stopped)");
                    cout<<" : " << c.GetEgg() << endl;
                }
            }
        }
        if(menu=="3"){ // stopped all chick
            if(pips.size()==0){cout<<"There are no chickens!"<<endl;}
            else {
                for (Chicken& c: pips) {
                    c.SetPoop(false);
                }
            }
        }
    }while(menu!="4"); // Stop and exit

    cout<<"Please wait! Do not disturb the pooping chickens!"<<endl;
    if(pips.size()>0){
        for (Chicken& c: pips) {
            c.SetPoop(false);
        }
    }

    for (auto& th : threads){
        th.join();
    }


    if(pips.size()>0){
        for (Chicken& c: pips) {
            c.Clean();
        }
    }

    printHello(false);


    return 0;
}
