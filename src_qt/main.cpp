#include <iostream>
#include <QString>
#include <QtCore/QTextStream>
#include <QtCore/QThreadPool>
#include "QChicken.h"

using namespace std;

void printhello(QTextStream &out,bool hello){
    QString h="   Hello Chickens!\n"
              "    _  /   _      _\n"
              " __(')= __(')> __(')<\n"
              " \\___)  \\___)  \\___)\n";



    QString b="   Bye Chickens!\n"
              "  _  /   _      _\n"
              "=(')__ <(')__ >(')__\n"
              " (___/  (___/  (___/\n";

    if(hello){
        out<<h;
    }
    else{
        out<<b;
    }
    out.flush();
}

int main(int argc, char *argv[])
{
    QTextStream out(stdout);
    QTextStream in(stdin);
    printhello(out,true);

    QString menu="";
    vector<QChicken*> pips;
    int i=1;

    do {
        out << endl<<"Please select one of: " << endl;
        out << "1 : Plant chicken" << endl;
        out << "2 : Status" << endl;
        out << "3 : Stop the hens" << endl;
        out << "4 : Shoo!" << endl;
        in >> menu;

        if(menu=="1"){ // Spawn 2-10 sec pooping chicken
            QString name ="QChicken_"+QString::number(i);
            QChicken *pip = new QChicken(name);
            QThreadPool::globalInstance()->start(pip);
            pips.push_back(pip);
            i++;
        }
        if(menu=="2"){ // Get chikens status
            out<<"QChicken    Status   Eggs"<<endl;
            if(pips.size()==0){out<<"There are no chickens!"<<endl;}
            else {
                for (QChicken* c: pips) {
                    out << c->GetName()<<" "<<(c->IsPoop()?"(pooping)":"(stopped)");
                    out<<" : " << c->GetEgg() << endl;
                }
            }
        }
        if(menu=="3"){ // stopped all chick
            if(pips.size()==0){out<<"There are no chickens!"<<endl;}
            else {
                for (QChicken* c: pips) {
                    c->SetPoop(false);
                }
            }
        }
    }while(menu!="4"); // Stop and exit

    out<<"Please wait! Do not disturb the pooping chickens!"<<endl;
    if(pips.size()>0){
        for (QChicken* c: pips) {
            c->SetAlive(false);
        }
    }

    QThreadPool::globalInstance()->waitForDone();


    printhello(out,false);


    return 0;
}