//
// Created by aniko on 2020. 03. 04..
//

#ifndef CHICKEN_QCHICKEN_H
#define CHICKEN_QCHICKEN_H

#include <iostream>
#include <QtCore/QString>
#include <QtCore/QRunnable>

using namespace std;

class QChicken : public QRunnable{
public:
    QChicken(QString cname, bool test=false);

    QString GetName();
    int GetRand();
    bool IsPoop();
    int GetEgg();
    void SetPoop(bool poop);
    void SetAlive(bool live);

protected:
    void run() override;
private:
    QString name;
    int  eggs = 0;
    bool ispoop=false;
    bool alive=true;
    bool testcase=false;
};

#endif //CHICKEN_QCHICKEN_H
