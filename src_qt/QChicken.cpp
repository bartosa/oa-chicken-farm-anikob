//
// Created by aniko on 2020. 03. 04..
//

#include "QChicken.h"
#include <QDebug>
#include <QThread>
#include <unistd.h>

QChicken::QChicken(QString name, bool test)
        : name(name)
{
    alive=true; // Schöninger's farm
    testcase=test;
}


QString QChicken::GetName() {
    return name;
}

int QChicken::GetRand(){

    qsrand(time(NULL));
    return (qrand() % 8 + 3);
}

void QChicken::run()
{
    ispoop=true;
    while(alive)
    {
        if(ispoop){
            int timeinterval = GetRand();
//         cout<<"Pooptime "<<name.toStdString()<<": "<<timeinterval<<endl;
        if(!testcase){
            QThread::sleep(timeinterval);
        }
        else{
            QThread::msleep(10);
        }
            eggs++;
//          cout<<"Poop by "+name.toStdString()<<": "<<eggs<<endl;
        }
    }
}

bool QChicken::IsPoop() {
    return ispoop;
}

int QChicken::GetEgg() {
    return eggs;
}

void QChicken::SetPoop(bool poop) {
    ispoop=poop;
}

void QChicken::SetAlive(bool live) {
    alive=live;
}
