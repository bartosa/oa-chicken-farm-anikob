//
// Created by aniko on 2020. 02. 07..
//
#include "src_cpp/Chicken.h"
#include "src_qt/QChicken.h"
#include <3rdparty/catch.hpp>
#include <iostream>
#include <string>
#include <mutex>
#include <QtCore/QThreadPool>

using namespace std;

TEST_CASE("Cpp") {
    Chicken c("pali");

    SECTION("QChicken name") {
        REQUIRE(c.GetName() == "pali");
    }

    SECTION("QChicken ispoop1") {
        REQUIRE(c.IsPoop() == false);
    }

    SECTION("QChicken ispoop2") {
        c.SetPoop(true);
        REQUIRE(c.IsPoop() == true);
    }

    SECTION("QChicken ispoop3") {
        c.SetPoop(false);
        REQUIRE(c.IsPoop() == false);
    }

    SECTION("QChicken getegg1") {
        REQUIRE(c.GetEgg() == 0);
    }

    SECTION("QChicken spawn") {
        mutex m;
        std::thread t1(&Chicken::Spawn, c, &m, true); // In test mode chicken poop 10 millisec
        this_thread::sleep_for(chrono::milliseconds (10));
        c.SetPoop(false);
        t1.join();
        REQUIRE(c.GetEgg() == 1);
    }
}

TEST_CASE("Qt") {
    QChicken *qpip = new QChicken("pali",true);
    SECTION("QChicken name") {
        REQUIRE(qpip->GetName() == "pali");
    }

    SECTION("Rand") {
        int num = qpip->GetRand();
        cout<<"Random num: "<<num<<endl;
        REQUIRE(num >= 2 );
        REQUIRE(num <=10 );
    }

    SECTION("QChicken ispoop1") {
        REQUIRE(qpip->IsPoop() == false);
    }

    SECTION("QChicken ispoop2") {
        qpip->SetPoop(true);
        REQUIRE(qpip->IsPoop() == true);
    }

    SECTION("QChicken ispoop3") {
        qpip->SetPoop(false);
        REQUIRE(qpip->IsPoop() == false);
    }
    SECTION("QChicken getegg1") {
        REQUIRE(qpip->GetEgg() == 0);
    }

    SECTION("QChicken spawn") {
        QThreadPool::globalInstance()->start(qpip);
        this_thread::sleep_for(chrono::milliseconds (10));
        qpip->SetAlive(false);
        QThreadPool::globalInstance()->waitForDone();
        REQUIRE(qpip->GetEgg() == 1);
    }

}